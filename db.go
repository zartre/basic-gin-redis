package main

import (
	"context"
	"github.com/go-redis/redis/v8"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
)

var redisClient *redis.Client
var mongoClient *mongo.Client
var mongoCtx, _ = context.WithTimeout(context.Background(), 10*time.Second)

func init() {
	redisClient = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_ADDRESS"),
		Password: "",
		DB:       0,
	})
	mongoClient, _ = mongo.Connect(mongoCtx, options.Client().ApplyURI(os.Getenv("MONGO_ADDRESS")))
}

func GetRedisClient() *redis.Client {
	return redisClient
}

func GetMongoClient() *mongo.Client {
	return mongoClient
}
