module basic-gin-redis

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis/v8 v8.0.0-beta.5
	go.mongodb.org/mongo-driver v1.3.4
)
