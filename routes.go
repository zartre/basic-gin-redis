package main

func initRoutes() {
	router.GET("/ping", pingHandler)

	router.GET("/name/:name", greetHandler)

	connectRoutes := router.Group("/connect")
	{
		connectRoutes.POST("/redis", redisHandler)
		connectRoutes.POST("/mongodb", mongoHandler)
	}
}
