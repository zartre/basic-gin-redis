package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

func pingHandler(c *gin.Context) {
	c.JSON(200, gin.H{
		"status": "pong",
	})
}

func greetHandler(c *gin.Context) {
	name := c.Param("name")
	c.JSON(200, gin.H{
		"hello": name,
	})
}

func mongoHandler(c *gin.Context) {
	// Map body to a map
	postData := map[string]string{
		"name": "",
	}
	_ = c.BindJSON(&postData)
	// Write to Mongo
	collection := GetMongoClient().Database("tut").Collection("people")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	_, err := collection.InsertOne(ctx, bson.M{"name": postData["name"]})
	// Respond
	status := "success"
	if err != nil {
		status = "failed"
	}
	c.JSON(200, gin.H{
		"status": status,
	})
}

func redisHandler(c *gin.Context) {
	// Map body to a map
	postData := map[string]string{
		"name": "",
	}
	_ = c.BindJSON(&postData)
	// Write to Redis
	err := GetRedisClient().Set(context.Background(), "name", postData["name"], 0).Err()
	status := "success"
	if err != nil {
		status = "failed"
	}
	// Respond
	c.JSON(200, gin.H{
		"status": status,
	})
}
