# Basic Gin Redis

A sample project with Redis and MongoDB connection.

Assign `MONGO_ADDRESS` and `REDIS_ADDRESS` environment variables before running.
